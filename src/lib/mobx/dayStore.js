// Core
import { makeAutoObservable } from 'mobx';

class DayStore {
    dayId = null;
    dayName = '';
    dayRainProbability = null;
    dayHumidity = null;
    dayTemperature = null;
    dayType = ''


    constructor(rootStore) {
        makeAutoObservable(this, { rootStore: false }, {
            autoBind: true,
        });
        this.rootStore = rootStore;
    }

    /* eslint max-params: ["error", 6] */

    setSelectedDay(id, day, rain_probability, humidity, temperature, type) {
        this.dayId = id;
        this.dayName = day;
        this.dayRainProbability = rain_probability;
        this.dayHumidity = humidity;
        this.dayTemperature = temperature;
        this.dayType = type;
    }

    resetSelectedDay() {
        this.dayId = null;
        this.dayName = '';
        this.dayRainProbability = null;
        this.dayHumidity = null;
        this.dayTemperature = null;
        this.dayType = '';
    }
}

export { DayStore };
