// Core
import { makeAutoObservable } from 'mobx';

// Store
import { DayStore } from './dayStore';
import { WeatherStore } from './weatherStore';

export class RootStore {
    dayStore = null;
    weatherStore = null;
    constructor() {
        makeAutoObservable(this);
        this.dayStore = new DayStore(this);
        this.weatherStore = new WeatherStore(this);
    }
}
