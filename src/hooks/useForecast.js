// Core
import { useQuery } from 'react-query';
import { api } from '../api';

export const useForecast = () => {
    const query = useQuery('forecast', api.getWeather);
    const { data, isFetched } = query;

    return {
        data: Array.isArray(data) ? data : [],
        isFetched,
    };
};
