// Core
import { useContext } from 'react';

// Store
import { Context } from '../lib/storeContext';

export const useStore = () => {
    const store = useContext(Context);

    return store;
};
