import { observer } from 'mobx-react-lite';
import { useForm } from 'react-hook-form';
import { useState } from 'react';

import { useStore } from '../../hooks/useStore';

export const Filter = observer(() => {
    const form = useForm({
        defaultValues: { value: '' },
    });
    const { weatherStore } = useStore();
    const { isFiltered } = weatherStore;

    const [selected, setSelected] = useState('');
    const [isChange, setChange] = useState(false);

    const buttonJSX = !isFiltered ? (
        <>
            <button
                type = 'submit'
                disabled = { !selected && !isChange }>Отфильтровать</button>
        </>
    ) : (
        <>
            <button
                type = 'submit'
                onClick = { () => {
                    setSelected('');
                    setChange(false);
                    form.reset();
                    weatherStore.resetFilter();
                } }>Сбросить</button>
        </>
    );


    return (
        <form
            className = 'filter' onSubmit = { form.handleSubmit((data) => {
                weatherStore.applyFilter(data);
            })
            }>
            <span
                className = { `checkbox ${selected === 'cloudy' ? 'selected' : ''} ${isFiltered ? 'blocked' : ''}`   }
                onClick = { () => {
                    form.setValue('type', 'cloudy');
                    setSelected('cloudy');
                }
                }>Облачно</span>
            <span
                className = { `checkbox ${selected === 'sunny' ? 'selected' : ''} ${isFiltered ? 'blocked' : ''}`   }
                onClick = { () => {
                    form.setValue('type', 'sunny');
                    setSelected('sunny');
                } }>Солнечно</span>
            <p className = 'custom-input'>
                <label htmlFor = 'min-temperature'>Минимальная температура</label>
                <input
                    id = 'min-temperature'
                    type = 'number'
                    { ...form.register('minTemperature') }
                    disabled = { isFiltered }
                    onChange = { () => setChange(true) } />
            </p>
            <p className = 'custom-input'>
                <label htmlFor = 'min-temperature'>Максимальная температура</label>
                <input
                    id = 'max-temperature'
                    type = 'number'
                    { ...form.register('maxTemperature') }
                    disabled = { isFiltered }
                    onChange = { () => setChange(true) } />
            </p>
            { buttonJSX }
        </form>
    );
});
