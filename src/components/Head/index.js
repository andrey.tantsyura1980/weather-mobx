// Core
import format from 'date-fns/format';
import ru from 'date-fns/locale/ru';
import { observer } from 'mobx-react-lite';

// Hooks
import { useStore } from '../../hooks/useStore';

export const Head = observer(() => {
    const { dayStore } = useStore();

    const {
        dayName, dayRainProbability, dayHumidity, dayTemperature, dayType,
    } = dayStore;


    const dayWeekday = dayName ? format(new Date(dayName), 'eeee', { locale: ru }) : null;
    const dayShortForm = dayName ? format(new Date(dayName), 'd MMMM', { locale: ru }) : null;

    return dayName ? (
        <>
            <div className = 'head'>
                <div className = { `icon ${dayType}` }></div>
                <div className = 'current-date'>
                    <p>{ dayWeekday }</p>
                    <span>{ dayShortForm }</span>
                </div>
            </div>
            <div className = 'current-weather'>
                <p className = 'temperature'>{ dayTemperature }</p>
                <p className = 'meta'>
                    <span className = 'rainy'>
                %
                        { dayRainProbability }</span>
                    <span className = 'humidity'>
                %
                        { dayHumidity }
                    </span>
                </p>
            </div>
        </>
    ) : null;
});
