// Core
import { observer } from 'mobx-react-lite';
import cx from 'classnames';
import { useEffect } from 'react';

// Instruments
import format from 'date-fns/format';
import ru from 'date-fns/locale/ru';
import { fetchify } from '../../helpers';

// Hooks
import { useStore } from '../../hooks/useStore';
import { useForecast } from '../../hooks';

export const Forecast = observer(() => {
    const { data, isFetched } = useForecast();
    const { dayStore, weatherStore } = useStore();
    const filteredData = weatherStore.filteredDays(data) || [];

    useEffect(() => {
        if (Array.isArray(filteredData) && filteredData?.length) {
            dayStore.setSelectedDay(
                filteredData[ 0 ].id,
                filteredData[ 0 ].day,
                filteredData[ 0 ].rain_probability,
                filteredData[ 0 ].humidity,
                filteredData[ 0 ].temperature,
                filteredData[ 0 ].type,
            );
        } else {
            dayStore.resetSelectedDay();
        }
    }, [filteredData]);

    const messageJSX = (
        <>
            <p className = 'message'>По заданным критериям нет доступных дней!</p>
        </>
    );

    const daysJSX = filteredData.map((days) => {
        const {
            id, day, rain_probability, humidity, temperature, type,
        } = days;

        const handleClick = () => {
            dayStore.setSelectedDay(id, day, rain_probability, humidity, temperature, type);
        };

        const settingClasses = cx('day', `${type}`, { selected: dayStore.dayId === id });

        return (
            <>
                <div
                    className = { settingClasses }
                    key = { days.id }
                    onClick = { handleClick }>
                    <p>{ format(new Date(day), 'eeee', { locale: ru }) }</p>
                    <span>{ temperature }</span>
                </div>
            </>
        );
    }).slice(0, 7);

    return (
        <div className = 'forecast'>
            { filteredData.length ? fetchify(isFetched, daysJSX) :  messageJSX }
        </div>
    );
});
