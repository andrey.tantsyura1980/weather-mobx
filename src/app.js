// Components
import {
    Filter, Head, Forecast,
} from './components';

// Instruments

export const App = () => {
    return (
        <main>
            <Filter />
            <Head />
            <Forecast />
        </main>
    );
};
