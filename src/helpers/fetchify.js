export const fetchify = (isFetched, content) => {
    if (!isFetched) {
        return <p style = { { color: 'white' } }>Loading...</p>;
    }

    if (content) {
        return content;
    }

    return null;
};
